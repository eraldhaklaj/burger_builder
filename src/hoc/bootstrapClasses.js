import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';

const bootstrapClasses = (classesString) => {
    const classes = classesString.split(' ').map(btnClass => {
        return bootstrap[btnClass];
    }).join(' ');

    return classes
};

export default bootstrapClasses;
import React, { Component } from 'react';

import Order from '../../components/Order/Order';

import bootstrapClasses from '../../hoc/bootstrapClasses';
import axios from '../../axios-orders';

class Orders extends Component {
    state = {
        orders: [],
        loading: true
    }

    componentDidMount() {
        axios.get('/orders.json').then(res => {
            const fetchOrders = [];
            for (let key in res.data) {
                fetchOrders.push({
                    ...res.data[key],
                    id: key
                });
            }
            this.setState({loading: false, orders: fetchOrders});
        })
        .catch(err => {
            this.setState({loading: false});
        });
    }

    render () {
        return (
            <div>
                <br/>
                <br/>
                <div className={bootstrapClasses('container')}>
                    <div className={bootstrapClasses('row')}>
                        <div className={bootstrapClasses('col-md-6 mx-auto')}>
                            {this.state.orders.map(order => {
                                console.log(order);
                                return <Order 
                                    key={order.id} 
                                    ingredients={order.ingredients}
                                    price={order.price}
                                    orderId={order.id} />
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Orders;
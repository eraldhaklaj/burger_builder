import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import Modal from '../../../components/UI/Modal/Modal';
import Backdrop from '../../../components/UI/Backdrop/Backdrop';

import bootstrapClasses from '../../../hoc/bootstrapClasses';
import * as actions from '../../../store/actions/index';

const inputClasses = bootstrapClasses('form-control mb-2');

class ContactData extends Component {
    state = {
        costumerDetails: {
            name: '',
            email: '',
            street: '',
            postalCode: '',
            country: '',
            deliveryMethod: 'fastest'
        },
        error: false
    }

    orderHandler = (event) => {
        event.preventDefault();
        const order = {
            ingredients: this.props.ings,
            price: this.props.price,
            costumer: this.state.costumerDetails
        }
        
        this.props.onOrderBurger(order);
    }

    inputChangedHandler = (event) => {
        const current = this.state.costumerDetails;
        current[event.target.id] = event.target.value;
        this.setState({costumerDetails: {...current}});
        
    }

    closeModalHandler = () => {
        this.setState( { error: false } );
    }

    render() {
        const output = () => {
            return (
                <div className={bootstrapClasses('container my-4')}>
                    <div className={bootstrapClasses('row justify-content-center')}>
                        <div className={bootstrapClasses('col-md-6')}>
                            <div className={bootstrapClasses('card border-0 shadow')}>
                                <div className={bootstrapClasses('card-header border-0')}>
                                    <h4 
                                        className={bootstrapClasses('text-center card-title')}>
                                        Enter your Contact Data
                                    </h4>
                                </div>
                                <Backdrop show={this.state.error} modalClosed={this.closeModalHandler} />
                                <Modal show={this.state.error}>
                                    <div className={bootstrapClasses('alert alert-warning mb-3')}>Network Error!</div>
                                </Modal>
                                <div className={bootstrapClasses('card-body')}>
                                    <form className={bootstrapClasses('d-flex flex-column')} onSubmit={this.orderHandler}>

                                        <label htmlFor="name">Your Name</label>
                                        <input type="text" name="name" placeholder="Your Name" id="name" className={inputClasses} onChange={(event) => this.inputChangedHandler(event)} required/>

                                        <label htmlFor="street">Street</label>
                                        <input type="text" name="street" placeholder="Your Street" id="street" className={inputClasses} onChange={(event) => this.inputChangedHandler(event)} required/>

                                        <label htmlFor="postalCode">ZIP Code</label>
                                        <input type="text" name="postal" placeholder="ZIP Code" id="postalCode" className={inputClasses} onChange={(event) => this.inputChangedHandler(event)} required/>
                                        
                                        <label htmlFor="country">Your Country</label>
                                        <input type="text" name="country" placeholder="Country" id="country" className={inputClasses} onChange={(event) => this.inputChangedHandler(event)} required/>
                                        
                                        <label htmlFor="email">Your Email</label>
                                        <input type="email" name="email" placeholder="Your Email" id="email" className={inputClasses} onChange={(event) => this.inputChangedHandler(event)} required/>

                                        <label htmlFor="deliveryMethod">Delivery Method</label>
                                        <select name="method" id="deliveryMethod" className={inputClasses} onChange={(event) => this.inputChangedHandler(event)} required>
                                            <option value="fastest">Fastest</option>
                                            <option value="cheapest">Cheapest</option>
                                        </select>

                                        <Button btnType="btn btn-primary align-self-end" event={null} disabled={this.props.loading}>ORDER</Button>

                                    </form>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
            );
        }

        return output();
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        loading: state.order.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onOrderBurger: (orderData) => dispatch(actions.purchaseBurger(orderData))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactData);
import React, { Component } from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import CheckoutSummary  from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData  from './ContactData/ContactData';

class Checkout extends Component {

    orderContinuedHandler = () => {
        this.props.history.push('/checkout/contact-data');
    }

    orderCanceledHandler = () => {
        this.props.history.goBack();
    }

    render() {
        let summary = <Redirect to="/" />;
        if(this.props.ings) {
            const purchasedRedirect = this.props.purchased ? <Redirect to ="/" /> : null;
            summary = (
                <div>
                    {purchasedRedirect}
                    <CheckoutSummary 
                        ingredients={this.props.ings}
                        orderCanceledHandler={this.orderCanceledHandler}
                        orderContinuedHandler={this.orderContinuedHandler} />
                    <Route 
                        path={this.props.match.path + '/contact-data'}
                        component={ContactData} />
                </div>
            );
        }
        return summary;
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        purchased: state.order.purchased
    };
};

export default connect(mapStateToProps)(withRouter(Checkout));
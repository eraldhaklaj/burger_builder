import React, {Component} from 'react';
import { connect } from 'react-redux';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import bootstrapClasses from '../../hoc/bootstrapClasses';
import * as actions from '../../store/actions/index';

const errorClasses = bootstrapClasses('alert alert-warning w-50 mx-auto text-center');
const loadingClasses = bootstrapClasses('alert alert-info w-50 mx-auto text-center');
const beforeLoadingStyles = {
    marginTop: '30vh'
};

class BurgerBuilder extends Component {
    state = {
        purchasing: false,
        processing: false
    }

    componentDidMount () {
        this.props.onInitIngredients();
    }

    updatePurchaseState = (ingredients) => {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey] 
            })
            .reduce((sum, el) => {
                return sum + el;
            });
        return sum > 0;
    }

    purchaseHandler = () => {
        this.setState({purchasing: true});
    }

    purchaseCancelHandler = () => {
        this.setState({purchasing: false});
        console.log(this.state);
    }

    purchaseContinueHandler = () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    getIngredientsHandler = () => {
        
    }

    render() {
        const disabledInfo = {
            ...this.props.ings
        }
        for(let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }
        let burger = this.props.error ? 
            <div className={errorClasses} style={beforeLoadingStyles}><h3>Ingredients can't be loaded</h3></div> : <div className={loadingClasses} style={beforeLoadingStyles}><h3>Preparing Ingredients</h3></div>;

        if(this.props.ings) {
            burger = <div>
                <Backdrop show={this.state.purchasing} modalClosed={this.purchaseCancelHandler} />
                <Modal show={this.state.purchasing} >
                    <OrderSummary 
                        ingredients={this.props.ings} 
                        price={this.props.price}
                        modalClosed={this.purchaseCancelHandler}
                        purchaseContinued={this.purchaseContinueHandler}
                        processing={this.state.processing}>
                    </OrderSummary>
                </Modal>
                <Burger ingredients={this.props.ings}/>
                <BuildControls 
                    ingredientAdded={this.props.onIngredientAdded}
                    ingredientremoved={this.props.onIngredientRemoved}
                    disabled={disabledInfo}
                    purchasable={this.updatePurchaseState(this.props.ings)}
                    ordered={this.purchaseHandler}
                    price={this.props.price} />
            </div>
        }
        return burger;
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (ingName) => dispatch(actions.addIngredient(ingName)),
        onIngredientRemoved: (ingName) => dispatch(actions.removeIngredient(ingName)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchase: () => dispatch(actions.purchaseInit())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);
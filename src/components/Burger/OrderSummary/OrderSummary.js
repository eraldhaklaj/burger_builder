import React, { Component } from 'react';

import Button from '../../UI/Button/Button';
import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';

class OrderSummary extends Component {
    render() {
        // COMPONENT CLASSES
        const ulClasses = [bootstrap['list-group']].join(' ');
        const liClasses = [bootstrap['list-group-item'], bootstrap['d-flex'], bootstrap['justify-content-between']].join(' ');
        const spanClasses = [bootstrap['badge'], bootstrap['badge-pill'], bootstrap['bg-primary'], bootstrap['d-flex'], bootstrap['align-items-center'], bootstrap['text-white']].join(' ');
        const buttonWrapperClasses = [bootstrap['d-flex'], bootstrap['justify-content-end']].join(' ');
        // const errorClasses = [bootstrap['alert'], bootstrap['alert-danger'], bootstrap['mt-2']].join(' ');
        


        const ingredientSummary = Object.keys(this.props.ingredients)
            .map((igKey, index) => {
                return <li key={igKey + index} className={liClasses}>{igKey} <span className={spanClasses}>{this.props.ingredients[igKey]}</span></li>
            });

        return (
            <div>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingredients:</p>
                <ul className={ulClasses}>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: ${parseFloat(this.props.price).toFixed(2)}</strong></p>
                <p>Continue to Checkout</p>
                <div className={buttonWrapperClasses}>
                    <Button 
                        event={this.props.modalClosed}
                        btnType={"btn btn-outline-secondary btn-sm mr-1"}>
                        CANCEL
                    </Button>
                    <Button 
                        event={this.props.purchaseContinued} 
                        disabled={this.props.processing}
                        btnType={"btn btn-primary btn-sm"}>
                        CONTINUE
                    </Button>
                </div>
                {/* {this.props.children !== null ? <div className={errorClasses}>{this.props.children}</div> : null} */}
            </div>
        );
    }
}

export default OrderSummary;
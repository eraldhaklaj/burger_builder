import React, { PureComponent } from 'react';

import classes from './Modal.css';



class Modal extends PureComponent {

    render() {
        return <div>
            <div 
                className={classes.Modal}
                style={{
                    transform: this.props.show ? 'translateY(0)' : 'translateY(100vh)',
                    opacity: this.props.show ? '1': '0'
                }}>
                {this.props.children}
            </div>
        </div>
    }
}

export default Modal;
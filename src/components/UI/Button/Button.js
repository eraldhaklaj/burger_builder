import React from 'react';

import bootstrapClasses from '../../../hoc/bootstrapClasses';

const button = (props) => {
    // COMPONENT CLASSES
    const buttonClasses = bootstrapClasses(props.btnType);

    return (
        <button 
            className={buttonClasses} 
            onClick={props.event}
            disabled={props.disabled}>
            {props.children}
        </button>
    );
};

export default button;
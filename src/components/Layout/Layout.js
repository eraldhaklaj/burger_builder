import React from 'react';

import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import classes from './Layout.css';

const layout = (props) => {
    return <div>
        <Toolbar />
        <main 
            className={classes.Content} 
            style={{marginTop: '56px'}}>
            {props.children}
        </main>
    </div>
}

export default layout;
import React from 'react';
import { NavLink } from 'react-router-dom';

import classes from './NavigationItems.css';
import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';

//COMPONENTS CLASSES
const ulWrapperClasses = [bootstrap['ml-auto'], bootstrap['d-flex']].join(' ');
const ulClasses = [bootstrap['navbar-nav'], bootstrap['d-flex'], bootstrap['flex-row']].join(' ');
const liClasses = [bootstrap['nav-item'], bootstrap['px-2'], bootstrap['d-flex'], bootstrap['align-items-center']].join(' ');
const aClasses = [bootstrap['nav-link']].join(' ');

const navigationItems = () => (
    <div className={ulWrapperClasses}>
        <ul className={ulClasses}>
            <li className={[liClasses].join(' ')}>
                <NavLink to="/" exact activeClassName={classes.ActiveLink} className={[aClasses].join(' ')}>Burger Builder</NavLink>
            </li>
            <li className={liClasses}>
                <NavLink to="/checkout" activeClassName={classes.ActiveLink} className={aClasses} href="/checkout">Checkout</NavLink>
            </li>
            <li className={liClasses}>
                <NavLink to="/orders" activeClassName={classes.ActiveLink} className={aClasses} href="/orders">Orders</NavLink>
            </li>
        </ul>
    </div>
);

export default navigationItems;
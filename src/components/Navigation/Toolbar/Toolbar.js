import React from 'react';

import Logo from '../../Logo/Logo';
import NavigationItems from '../../Navigation/NavigationItems/NavigationItems';
import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';

// COMPONENT CLASSES
const headerClasses = [bootstrap.navbar, bootstrap['navbar-light'], bootstrap['bg-warning'], bootstrap['fixed-top'], bootstrap['p-0'], bootstrap['align-items-stretch']].join(' ');
const toolbar = (props) => (
    <header className={headerClasses}>
        <Logo />
        <NavigationItems />
    </header>
);

export default toolbar;
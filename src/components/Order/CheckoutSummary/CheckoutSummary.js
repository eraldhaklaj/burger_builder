import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

import bootstrapClasses from '../../../hoc/bootstrapClasses';

// COMPONENT CLASSES
const wrapperClasses = bootstrapClasses('d-flex flex-column align-items-center');
const buttonWrapperClasses = bootstrapClasses('mt-3');

const checkoutSummary = (props) => {

    return (
        <div className={wrapperClasses}>
            <h1>We hope it tastes well!</h1>
            <Burger ingredients={props.ingredients} />
            <div className={buttonWrapperClasses}>
                <Button btnType={"btn btn-outline-secondary btn-sm mr-1"} event={props.orderCanceledHandler}>
                    CANCEL
                </Button>
                <Button btnType={"btn btn-primary btn-sm"} event={props.orderContinuedHandler}>
                    CONTINUE
                </Button>
            </div>
        </div>
    );
}

export default checkoutSummary;
import React from 'react';

import bootstrapClasses from '../../hoc/bootstrapClasses';

const order = (props) => {
    const ingredients = [];

    for (let ingredientName in props.ingredients) {
        ingredients.push({
            name: ingredientName,
            amount: props.ingredients[ingredientName]
        });
    }

    const ingredientOutput = ingredients.map(ig => {
        return <li 
            className={bootstrapClasses('list-group-item d-flex justify-content-between border-0')}>
            {ig.name}
            <span 
                className={bootstrapClasses('badge badge-pill bg-primary d-flex align-items-center text-white')}>
                {ig.amount}
            </span>
        </li>
    })

    return <div className={bootstrapClasses('card border-0 shadow mb-3')}>
            <div className={bootstrapClasses('card-header border-0 text-center')}>
                <h4 className={bootstrapClasses('card-title')}>Order {props.orderId}</h4>
            </div>
            <div className={'card-body'}>
                <ul className={bootstrapClasses('list-group mb-3 border-0')}>
                    <li 
                        className={bootstrapClasses('list-group-item d-flex justify-content-center border-0')}>
                        <h6>Ingredients</h6>
                    </li>
                    {ingredientOutput}
                    <li 
                        className={bootstrapClasses('list-group-item d-flex justify-content-between border-0')}>
                        Price: 
                        <span 
                            className={bootstrapClasses('badge badge-pill bg-primary d-flex align-items-center text-white')}>
                            USD {parseFloat(props.price).toFixed(2)}
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        
};

export default order;
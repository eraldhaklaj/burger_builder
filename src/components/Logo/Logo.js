import React from 'react';

import burgerLogo from '../../assets/images/logo.png';
import classes from './Logo.css';
import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';

//COMPONENTS CLASSES
const imgClasses = [bootstrap['h-100'], classes.Logo].join(' ');
const brandClasses = [bootstrap['navbar-brand'], bootstrap['p-0'], bootstrap['px-3'], bootstrap['py-2']].join(' ');

const logo = (props) => (
    <div>
        <a className={brandClasses} href="/">
            <img 
                src={burgerLogo} 
                alt="Logo"
                className={imgClasses}
                style={{maxHeight: '40px'}} />
        </a>
    </div>
);

export default logo;